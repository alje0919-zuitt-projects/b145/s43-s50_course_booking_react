import logo from './logo.svg';
import './App.css';

// remove the template in the module, to give way to the new line of code that we will create for the app.

// when compiling multiple components, they should be wrapped inside a single element.

// save an image of your favorite cartoon character inside the public folder

// NEW SKILL: identify how to access resources and elements from the public folder/library of the project.

// Public -> store all documents and modules that are shared accross all components of the app that are visible to all.

// to acquire the image that you want to display, you have to get it from the public folder. import the element inside this module.
import cartoon from './mir4Lancer.png';
// pass down the alias that identifies the resource into our element.

function App() {
  return (
    <div>
      <h1>Welcome to the course booking batch 164</h1>
      <h5>This is our project in React</h5>
      <h6>Come Visit our website</h6>
      {/*This image came from the /src folder*/}
      <img src={cartoon} alt="image not found" height="600"/>

      <img src="mir4Lancer.png" alt="image not found" height="600"/>

      {/*Lets try to display an image but this time it will come from the public folder*/}
      <h3>This is my favorite cartoon character</h3>
    </div>    
  );
}

export default App;
